#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Shader.h"
#include "Camera.h"
#include "Model.h"

#include <iostream>

/*
COMENZI AVION :
1,2,3 = CAMERA POSITION
4 = FIRST PLANE
5 = SECOND ONE
6 = BOTH OF THEM
O = START FLYING AIRPLANE 1 AND 2
P = STOP THEM

Y = START FLYING AIRPLANE 3
U = STOP IT

Q = RESET POSITION FOR 1,2,3

R = RESET POSTION OF CAMERA AND AIRPLANE 1 AND 2

Z = RESET POSITION OF ONLY AIRPLANE 3


*/




void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
//void processInput(GLFWwindow *window);
void processInput(GLFWwindow* window);

// settings
const unsigned int SCR_WIDTH = 1920;
const unsigned int SCR_HEIGHT = 1080;

//functions

void renderFar(const Shader& ourShader, Model& test);
void renderFloor();
void renderGrass(const Shader& ourShader, Model& grass);
void renderGrassGOOD(const Shader& ourShader, Model& grass, float zPosition, float xPosition, float diference, int HowManyTimes);
void renderRoad(const Shader& ourShader, Model& road, Model& ramp);
void RenderAirplane(const Shader& shader, Model& avion, float Scale, float zMoveAirplane, float xRotateAirplane, float yMoveAirplane, float xMoveAirplane);
void SIMULARE(const Shader& shader, Model& avion);
void SIMULARE2(const Shader& shader, Model& avion);
void SIMULARE3(const Shader& shader, Model& avion, float& startFly3);
void SIMULARE4(const Shader& shader, Model& avion, float& startFly3);
unsigned int CreateTexture(const std::string& strTexturePath);

// camera
Camera* pCamera = nullptr;
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

//CHOSE your airplane
int airplaneToChose = 0;
//AIRPLANE SCALE
float scaleAirplane = 1.40f;
float scaleAirplane3 = 1.50f;


//airplane1 move
float zMoveAirplane = -150.0f;
float xRotateAirplane = -90.0f;
float yMoveAirplane = 0.0f;
float xMoveAirplane = 7.5f;
int startFly = 0;

//airplane2 move
float zMoveAirplane2 = 0.0f;
float xRotateAirplane2 = -90.0f;
float yMoveAirplane2 = 0.0f;
float xMoveAirplane2 = 7.5f;
int startFly2 = 0;

//airplane3 move
float zMoveAirplane3 = 150.0f;
float xRotateAirplane3 = -90.0f;
float yMoveAirplane3 = 3.5f;
float xMoveAirplane3 = 7.5f;
float startFly3 = 0;
float startFly3Aux = 1;

//grass coord
float xLeftGrass = -205.0f;
float xRightGrass = 205.0f;



int main()
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "AEROPORT G3D", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// Create camera
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0f, 5.0f, 200.0f)); //pozitia 1


	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	glEnable(GL_DEPTH_TEST);

	Shader ourShader("VertexShader.vs", "FragmentShader.fs");
	Shader shadowMappingShader("ShadowMapping.vs", "ShadowMapping.fs");
	Shader shadowMappingDepthShader("ShadowMappingDepth.vs", "ShadowMappingDepth.fs");

	Model airplane1("model/airplane2/B_787_8.3ds");
	//Model airplane2("model/airplane2/B_787_8.3ds");
	//Model airplane3("model/airplane2/B_787_8.3ds");
	Model grass("model/grass/grass.obj");
	Model road("model/road/road.blend");
	Model testModel("model/far/faro.3ds");
	Model ramp("model/testModel/16309_SkatePark_wedgeramp_v1.obj");

	unsigned int floorTexture = CreateTexture("490.jpg");
	//unsigned int roadTexture = CreateTexture("blackTexture.jpg");



	glEnable(GL_CULL_FACE);

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;


		processInput(window);


		glClearColor(0.0f, 0.4f, 0.6f, 1.0f);  //SKY COLORs
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glClear(GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		ourShader.use();

		glm::mat4 model = glm::mat4(1.0f);


		glm::mat4 projection = pCamera->GetProjectionMatrix();
		glm::mat4 view = pCamera->GetViewMatrix();
		ourShader.setMat4("projection", projection);
		ourShader.setMat4("view", view);

		// floor
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		glActiveTexture(GL_TEXTURE1);
		glDisable(GL_CULL_FACE);
		//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1, 0, 0));
		model = glm::scale(model, glm::vec3(20.0f, 0.0f, 100.0f));
		ourShader.setMat4("model", model);
		renderFloor();

		//AIRPLANES
		RenderAirplane(ourShader, airplane1, scaleAirplane, zMoveAirplane, xRotateAirplane, yMoveAirplane, xMoveAirplane);
		//RenderAirplane(ourShader, airplane2, scaleAirplane, zMoveAirplane2, xRotateAirplane2, yMoveAirplane2, xMoveAirplane2);
		//RenderAirplane(ourShader, airplane3, scaleAirplane3,zMoveAirplane3, xRotateAirplane3, yMoveAirplane3, xMoveAirplane3);

		//grass
		//renderGrass(ourShader, grass);
		renderGrassGOOD(ourShader, grass, -2510.0f, -205.0f, 90.0f, 57);
		renderGrassGOOD(ourShader, grass, -2510.0f, 205.0f, 90.0f, 57);

		renderGrassGOOD(ourShader, grass, -2510.0f, -295.0f, 90.0f, 57);
		renderGrassGOOD(ourShader, grass, -2510.0f, 295.0f, 90.0f, 57);

		renderGrassGOOD(ourShader, grass, -2510.0f, -295.0f, 90.0f, 57);
		renderGrassGOOD(ourShader, grass, -2510.0f, 295.0f, 90.0f, 57);

		renderGrassGOOD(ourShader, grass, -2510.0f, -385.0f, 90.0f, 57);
		renderGrassGOOD(ourShader, grass, -2510.0f, 385.0f, 90.0f, 57);

		renderGrassGOOD(ourShader, grass, -2510.0f, -445.0f, 90.0f, 57);
		renderGrassGOOD(ourShader, grass, -2510.0f, 445.0f, 90.0f, 57);


		//road
		renderRoad(ourShader, road, ramp);

		//New Objects

		renderFar(ourShader, testModel);

		//SIMULARE ZBOR
		SIMULARE(ourShader, airplane1);
		//SIMULARE2(ourShader, airplane2);

		//SIMULARE3(ourShader, airplane3,startFly3);
		//SIMULARE4(ourShader, airplane3,startFly3);




		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}

void SIMULARE(const Shader& shader, Model& avion)
{
	int count = 0;
	if (startFly == 1)
		while (count != 100 && startFly == 1)
		{
			zMoveAirplane += 0.030f;
			if (xRotateAirplane > -80.0f)
				xRotateAirplane += 0.005f;
			if (zMoveAirplane >= 45.0f)
				yMoveAirplane += 0.005f;
			RenderAirplane(shader, avion, scaleAirplane, zMoveAirplane, xRotateAirplane, yMoveAirplane, xMoveAirplane);
			if (zMoveAirplane >= 400.0f)
			{

				startFly = 2;
				xRotateAirplane = -90.0f;
				zMoveAirplane = 0.0f;
				yMoveAirplane = 0.0f;
				xMoveAirplane = 50.0f;
			}
			count++;
		}
}
void SIMULARE2(const Shader& shader, Model& avion)
{
	int count = 0;
	if (startFly2 == 1)
		while (count != 100 && startFly2 == 1)
		{
			zMoveAirplane2 -= 0.030f;
			if (xRotateAirplane2 < 100.0f)
				xRotateAirplane2 += 0.001f;
			if (zMoveAirplane2 <= -60.0f)
				yMoveAirplane2 += 0.005f;
			RenderAirplane(shader, avion, scaleAirplane, zMoveAirplane2, xRotateAirplane2, yMoveAirplane2, xMoveAirplane2);
			if (zMoveAirplane2 <= -400.0f)
			{

				startFly2 = 2;
				zMoveAirplane2 = 0.0f;
				xRotateAirplane2 = 90.0f;
				yMoveAirplane2 = 0.0f;
				xMoveAirplane2 = -50.0f;
			}
			count++;
		}
}
void SIMULARE3(const Shader& shader, Model& avion, float& startFly3)
{
	int count = 0;
	if (startFly3 == 1)
		while (count != 100 && startFly3 == 1)
		{

			zMoveAirplane3 -= 0.025f;
			RenderAirplane(shader, avion, scaleAirplane3, zMoveAirplane3, xRotateAirplane3, yMoveAirplane3, xMoveAirplane3);


			if (zMoveAirplane3 <= -600.0f)
			{
				xRotateAirplane3 = -90.0f;
				startFly3 = 3;
			}
			count++;
		}
}
void SIMULARE4(const Shader& shader, Model& avion, float& startFly3)
{
	int count = 0;
	if (startFly3 == 3)
	{
		xRotateAirplane3 = -90.0f;
		//RenderAirplane(shader, avion, zMoveAirplane3, xRotateAirplane3, yMoveAirplane3, xMoveAirplane3);
		while (count != 100 && startFly3 == 3)
		{

			zMoveAirplane3 += 0.025f;
			RenderAirplane(shader, avion, scaleAirplane3, zMoveAirplane3, xRotateAirplane3, yMoveAirplane3, xMoveAirplane3);
			if (zMoveAirplane3 >= 600.0f)
			{
				startFly3 = 1;
				xRotateAirplane3 = 90.0f;
			}
			count++;
		}
	}

}

void RenderAirplane(const Shader& shader, Model& avion, float Scale, float zMoveAirplane, float xRotateAirplane, float yMoveAirplane, float xMoveAirplane)
{

	glm::mat4 model;
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xMoveAirplane, yMoveAirplane, zMoveAirplane));
	model = glm::rotate(model, glm::radians(xRotateAirplane), glm::vec3(1, 0, 0));
	//model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1,0, 0));
	if (xRotateAirplane > 0.0f)
		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0, 1, 0));

	model = glm::scale(model, glm::vec3(Scale, Scale, Scale));
	shader.setMat4("model", model);
	avion.Draw(shader);


}

void renderRoad(const Shader& ourShader, Model& road, Model& ramp)
{
	glm::mat4 model;
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-45.0f, -2.5f, 0.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-45.0f, -2.5f, -80.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-45.0f, -2.5f, -160.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-45.0f, -2.5f, -205.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 530.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-49.5f, 0.0f, -120.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 1.25f, 0.6f));
	ourShader.setMat4("model", model);
	ramp.Draw(ourShader);


	//+

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-45.0f, -2.5f, 0.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-45.0f, -2.5f, 80.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-45.0f, -2.5f, 160.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-45.0f, -2.5f, 205.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 530.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);





	//PARTEA DREAPTA
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(45.0f, -2.5f, 0.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(45.0f, -2.5f, -80.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(45.0f, -2.5f, -160.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(45.0f, -2.5f, -205.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 530.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	//+

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(45.0f, -2.5f, 0.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(45.0f, -2.5f, 80.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(45.0f, -2.5f, 160.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 10.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(45.0f, -2.5f, 205.0f));
	model = glm::scale(model, glm::vec3(10.0f, 10.0f, 530.0f));
	ourShader.setMat4("model", model);
	road.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(49.5f, 0.0f, 120.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1, 0, 0));
	model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0, 1, 0));
	model = glm::scale(model, glm::vec3(0.3f, 1.25f, 0.6f));
	ourShader.setMat4("model", model);
	ramp.Draw(ourShader);

}

void renderFar(const Shader& ourShader, Model& test)
{
	glm::mat4 model;

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-115.0f, -2.5f, -0.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	ourShader.setMat4("model", model);
	test.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(115.0f, -2.5f, -0.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	ourShader.setMat4("model", model);
	test.Draw(ourShader);


	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-115.0f, -2.5f, -1800.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
	ourShader.setMat4("model", model);
	test.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(115.0f, -2.5f, -1800.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
	ourShader.setMat4("model", model);
	test.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-115.0f, -2.5f, 1800.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
	ourShader.setMat4("model", model);
	test.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(115.0f, -2.5f, 1800.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
	ourShader.setMat4("model", model);
	test.Draw(ourShader);
}
void renderGrassGOOD(const Shader& ourShader, Model& grass, float zPosition, float xPosition, float diference, int HowManyTimes)
{
	for (int i = 0; i <= HowManyTimes; i++)
	{
		glm::mat4 model;
		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(xPosition, 0.0f, zPosition)); //-110.0f;
		model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
		model = glm::scale(model, glm::vec3(0.33f, 1.3f, 0.3f));
		ourShader.setMat4("model", model);
		grass.Draw(ourShader);
		//diference
		zPosition = zPosition + diference;
	}
}
void renderGrass(const Shader& ourShader, Model& grass)
{
	glm::mat4 model;
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xLeftGrass, 0.0f, -110.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xLeftGrass, 0.0f, -200.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xLeftGrass, 0.0f, -20.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xLeftGrass, 0.0f, 70.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xLeftGrass, 0.0f, 160.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xLeftGrass, 0.0f, 205.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);



	//RIGHT 
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xRightGrass, 0.0f, -110.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xRightGrass, 0.0f, -200.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xRightGrass, 0.0f, -20.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xRightGrass, 0.0f, 70.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xRightGrass, 0.0f, 160.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(xRightGrass, 0.0f, 205.0f));
	model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	grass.Draw(ourShader);
}
void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		pCamera->ProcessKeyboard(FORWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		pCamera->ProcessKeyboard(BACKWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		pCamera->ProcessKeyboard(LEFT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		pCamera->ProcessKeyboard(RIGHT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
		pCamera->ProcessKeyboard(UP, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
		pCamera->ProcessKeyboard(DOWN, (float)deltaTime);

	if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)// PRESS 4 for airplane1
	{
		airplaneToChose = 1;
	}
	if (glfwGetKey(window, GLFW_KEY_5) == GLFW_PRESS)// PRESS 5 for airplane1
	{
		airplaneToChose = 2;
	}
	if (glfwGetKey(window, GLFW_KEY_6) == GLFW_PRESS)// PRESS 5 for airplane1
	{
		airplaneToChose = 3;
	}

	if (airplaneToChose == 1 || airplaneToChose == 3) {
		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
			int width, height;
			zMoveAirplane = 0.0f;
			xRotateAirplane = -90.0f;
			yMoveAirplane = 0.0f;
			xMoveAirplane = 50.0f;
			glfwGetWindowSize(window, &width, &height);
			pCamera->Reset(width, height);
		}
		//RESET ONLY AIRPLANE POSITION
		if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS) {
			xRotateAirplane = -90.0f;
			zMoveAirplane = 0.0f;
			yMoveAirplane = 0.0f;

		}

		//AIRPLANE MOVE ON GROUND  // UP + AIRPLANE ROTATION + FORWARD
		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			zMoveAirplane += 5.0f;
			if (xRotateAirplane > -95.0f)
				xRotateAirplane -= 0.09f;
			if (zMoveAirplane >= 50.0f)
			{

				yMoveAirplane += 0.65f;
			}
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			zMoveAirplane -= 5.0f;
			if (xRotateAirplane <= -90.0f)
				xRotateAirplane += 0.09f;
			if (yMoveAirplane >= 0)
			{
				yMoveAirplane -= 0.65f;
			}
		}
		//AIRPLANE FLYING = T G  // FORWARD + BACK
		if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) {
			zMoveAirplane += 5.0f;
		}
		if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS) {
			zMoveAirplane -= 5.0f;
		}
		//ROTATE AIRPLANE = F H  // AIRPLANE ROTATION
		if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) {
			xRotateAirplane += 0.01f;
		}
		if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {
			xRotateAirplane -= 0.01f;
		}

		//START SIMULARE ZBOR
		if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS)
		{
			startFly = 1;
			//startFly2 = 1;

		}
		if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS)
		{
			startFly = 0;
			//startFly2 = 0;
		}

	}
	if (airplaneToChose == 2 || airplaneToChose == 3) {
		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
			int width, height;
			zMoveAirplane2 = 0.0f;
			xRotateAirplane2 = 90.0f;
			yMoveAirplane2 = 0.0f;
			xMoveAirplane2 = -50.0f;
			glfwGetWindowSize(window, &width, &height);
			pCamera->Reset(width, height);
		}
		//RESET ONLY AIRPLANE POSITION
		if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS) {
			zMoveAirplane2 = 0.0f;
			xRotateAirplane2 = 90.0f;
			yMoveAirplane2 = 0.0f;
			xMoveAirplane2 = -50.0f;

		}

		//AIRPLANE MOVE ON GROUND  // UP + AIRPLANE ROTATION + FORWARD
		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			zMoveAirplane2 -= 5.0f;
			if (xRotateAirplane2 < 95.0f)
				xRotateAirplane2 += 0.2f;
			if (zMoveAirplane2 <= 90.0f)
				yMoveAirplane2 += 0.5f;
		}
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			zMoveAirplane2 += 5.0f;
			if (xRotateAirplane2 >= 90.0f)
				xRotateAirplane2 -= 0.2f;
			if (yMoveAirplane2 >= 0)
				yMoveAirplane2 -= 0.5f;
		}
		//AIRPLANE FLYING = T G  // FORWARD + BACK
		if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) {
			zMoveAirplane2 -= 5.0f;
		}
		if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS) {
			zMoveAirplane2 += 5.0f;
		}
		//ROTATE AIRPLANE = F H  // AIRPLANE ROTATION
		if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) {
			xRotateAirplane2 += 0.01f;
		}
		if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {
			xRotateAirplane2 -= 0.01f;
		}

		//START SIMULARE ZBOR
		if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS)
		{
			startFly2 = 1;
			//startFly2 = 1;

		}
		if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS)
		{
			startFly2 = 0;
			//startFly2 = 0;
		}

	}

	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
	{
		startFly = 1;
		startFly2 = 1;

	}
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		startFly = 0;
		startFly2 = 0;
	}

	if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS)
	{
		startFly3 = startFly3Aux;



	}
	if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS)
	{
		if (startFly3 != 0)
			startFly3Aux = startFly3;
		startFly3 = 0;
	}

	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
	{
		startFly3 = 0;
		zMoveAirplane3 = 0.0f;
		xRotateAirplane3 = 90.0f;
		yMoveAirplane3 = 100.0f;
		xMoveAirplane3 = 0.0f;
	}
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
	{

		zMoveAirplane = 0.0f;
		xRotateAirplane = -90.0f;
		yMoveAirplane = 0.0f;
		xMoveAirplane = 50.0f;


		//airplane2 move
		zMoveAirplane2 = 0.0f;
		xRotateAirplane2 = 90.0f;
		yMoveAirplane2 = 0.0f;
		xMoveAirplane2 = -50.0f;

		//airplane3 move
		zMoveAirplane3 = 0.0f;
		xRotateAirplane3 = 90.0f;
		yMoveAirplane3 = 100.0f;
		xMoveAirplane3 = 0.0f;
		startFly3 = 0;

	}

	//PARCARE AVIOANE


	//POZITIONARE AVIOANE

	//CAMERA CONTROL
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) { //pozitia 1
		pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0f, 150.0f, 600.0f));
	}
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) { //pozitia 2

		pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(400.0f, 100.0f, 0.0f));
	}
	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) { //pozitia 3

		pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0f, 400.0f, 200.0f));
	}




}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	pCamera->MouseControl((float)xpos, (float)ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	pCamera->ProcessMouseScroll((float)yoffset);
}

unsigned int planeVAO = 0;
void renderFloor()
{
	unsigned int planeVBO;

	if (planeVAO == 0) {
		float planeVertices[] = {
			// positions            // normals         // texcoords
			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
			25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
		};
		// plane VAO
		glGenVertexArrays(1, &planeVAO);
		glGenBuffers(1, &planeVBO);
		glBindVertexArray(planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

unsigned int CreateTexture(const std::string& strTexturePath)
{
	unsigned int textureId = -1;

	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char* data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		std::cout << "Failed to load texture: " << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}